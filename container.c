#include "terminatorize.h"

static Status _ContainerCreate(Container ** poThis, ContainerType peType, size_t pnIdentity) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "_ContainerCreate: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( *poThis == NULL ? OK : ERROR, "_ContainerCreate: poThis must to point on NULL.\n" );

    Container * loThis = ALLOC(Container);

    CHECK_OR_RETURN_WITH_TRACE( loThis != NULL ? OK : ERROR, "_ContainerCreate: cannot allocate memory for Container structure.\n" );

    // Filling fields
    loThis->oParent = NULL;
    loThis->nIdentity = pnIdentity;
    loThis->eType = peType;
    loThis->nItems = 0;
    loThis->aItems = NULL;
    loThis->sName = NULL;

    *poThis = loThis;

    return OK;
}

static Status _ContainerDestroy(Container ** poThis) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "_ContainerDestroy: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( *poThis != NULL ? OK : ERROR, "_ContainerDestroy: poThis must not to point on NULL.\n" );

    Container * loThis = *poThis;
    size_t lnIndex = 0;

    if (loThis->nItems != 0) {

        for (lnIndex = 0; lnIndex < loThis->nItems; lnIndex ++) {
        	_ContainerDestroy(&(loThis->aItems[lnIndex]));
        }

        FREE(loThis->aItems);
    }

    FREE(loThis->sName);
    FREE(loThis);

    *poThis = NULL;

    return OK;
}

static Status _ContainerRemoveChild(Container * poThis, Container * poChild) {

	CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "_ContainerRemoveChild: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( poChild != NULL ? OK : ERROR, "_ContainerRemoveChild: poChild must not to point on NULL.\n" );

    size_t lnIndex = 0;

    if (poThis->nItems > 0) {

    	// Search for a child given
		for (lnIndex = 0; lnIndex < poThis->nItems; lnIndex ++) {
			if (poThis->aItems[lnIndex] == poChild) { break; }
		}

		// Destroying child
		CHECK_OR_RETURN(_ContainerDestroy(&poChild));

		// Shifting the rest
		for (lnIndex = lnIndex + 1; lnIndex < poThis->nItems; lnIndex ++) {
			poThis->aItems[lnIndex - 1] = poThis->aItems[lnIndex];
		}
		poThis->nItems--;

		// Dealing with the storage space. Freeing or reallocating.
		if (poThis->nItems == 0) {
			FREE(poThis->aItems);
		} else {
			poThis->aItems = realloc(poThis->aItems, sizeof(Container*) * poThis->nItems);
		}
    }

    return OK;
}

static Status _ContainerPrint(const Container * poThis, const size_t pnLevel) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex < pnLevel; lnIndex++) { printf("\t"); }

	switch (poThis->eType) {
		case CONTAINER_TYPE_ROOT: MESSAGE("[Root] %u ERRONEOUS TYPE\n", poThis->nIdentity); break;
		case CONTAINER_TYPE_STRIP: MESSAGE("[Strip] %u ERRONEOUS TYPE\n", poThis->nIdentity); break;
		case CONTAINER_TYPE_STRIP_H: MESSAGE("[Strip (H)] %u\n", poThis->nIdentity); break;
		case CONTAINER_TYPE_STRIP_V: MESSAGE("[Strip (V)] %u\n", poThis->nIdentity); break;
		case CONTAINER_TYPE_WINDOW: MESSAGE(poThis->sName == NULL ? "[Window] %u\n" : "[Window] %u `%s`\n", poThis->nIdentity, poThis->sName); break;
	}

	for (lnIndex = 0; lnIndex < poThis->nItems; lnIndex++) {
		CHECK_OR_RETURN( _ContainerPrint(poThis->aItems[lnIndex], pnLevel + 1) );
	}

	return OK;
}

static const size_t _HasOverlayingRects(const TextRect * poRect, const TextRect * paRects, const size_t pnRects) {

	size_t lnIndex = 0;
	size_t lnTotal = 0;

	for (lnIndex = 0; lnIndex<pnRects; lnIndex++) {

		const TextRect * loRect = &(paRects[lnIndex]);

		if (poRect == loRect) { continue; }

		if (TRUE
			&& (poRect->top.x1 >= loRect->top.x1)
			&& (poRect->top.y1 >= loRect->top.y1)
			&& (poRect->bottom.x2 <= loRect->bottom.x2)
			&& (poRect->bottom.y2 <= loRect->bottom.y2)
		) {
			lnTotal ++;
		}
	}

	return lnTotal;
}

static Status _GetRootRect(const TextRect * paRects, const size_t pnRects, const TextRect ** ppOResult, size_t * ppnResult ) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex<pnRects; lnIndex++) {
		if (!_HasOverlayingRects(&(paRects[lnIndex]), paRects, pnRects)) {

			(*ppOResult) = &(paRects[lnIndex]);
			(*ppnResult) = lnIndex;

			return OK;
		}
	}

	return ERROR;
}

static Status _FillContainer(const TextGrid * poGrid, Container ** poThis, const TextRect * poRect, size_t pnIdentity, const TextRect * paRects, const size_t pnRects) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex<pnRects; lnIndex++) {

		const TextRect * loRect = &(paRects[lnIndex]);

		if (loRect == poRect) { continue; }

		if (TRUE
			&& (poRect->top.x1 <= loRect->top.x1)
			&& (poRect->top.y1 <= loRect->top.y1)
			&& (poRect->bottom.x2 >= loRect->bottom.x2)
			&& (poRect->bottom.y2 >= loRect->bottom.y2)
		) {
			Container * loContainer = NULL;
			CHECK_OR_RETURN( _FillContainer(poGrid, &loContainer, loRect, lnIndex, paRects, pnRects) );

			// Multiple hangling containers are always strips. Typing (h or v) will be done later.
			if ((*poThis) == NULL ) {
				CHECK_OR_RETURN( _ContainerCreate(poThis, CONTAINER_TYPE_STRIP, pnIdentity) );
			}

			// Dealing with the storage space
			(*poThis)->aItems = realloc((*poThis)->aItems, sizeof(Container*) * ((*poThis)->nItems + 1));
			(*poThis)->aItems[(*poThis)->nItems++] = loContainer;
			loContainer->oParent = (*poThis);
		}
	}

	// If no nested containers found then it is a window.
	if ((*poThis) == NULL) {
		CHECK_OR_RETURN( _ContainerCreate(poThis, CONTAINER_TYPE_WINDOW, pnIdentity) );
	}

	// Possible cleanup of multiple instances due to first approach
	// of coverage test will be done later on a next stage.
	return OK;
}

typedef struct {
	Container * oContainer;
	size_t nDepth;
} _ContainerPathInfo;

static Status _FindAllIdentifiedContainers(Container * poRoot, _ContainerPathInfo* paPaths, size_t pnMaxPaths, size_t * pnPaths, size_t pnWanted, size_t pnDepth) {

	size_t lnIndex = 0;

	if (poRoot != NULL) {

		if (poRoot->nIdentity == pnWanted) {

			CHECK_OR_RETURN_WITH_MESSAGE( (*pnPaths) < pnMaxPaths ? OK : ERROR, "Cannot add optimize info: not enough space in optimization buffer (%u containers acquired).\n", *pnPaths ); \

			_ContainerPathInfo * loInfo = &(paPaths[(*pnPaths)++]);

			loInfo->oContainer = poRoot;
			loInfo->nDepth = pnDepth;
		}

		for (lnIndex = 0; lnIndex < poRoot->nItems; lnIndex++) {
			CHECK_OR_RETURN( _FindAllIdentifiedContainers( poRoot->aItems[lnIndex], paPaths, pnMaxPaths, pnPaths, pnWanted, pnDepth + 1 ) );
		}
	}

	return OK;
}

static Status _OptimizeContainer(const TextGrid * poGrid, Container * poThis, const size_t pnIdentities) {

	const size_t lnMaxRunInfo = pnIdentities * ((pnIdentities / 2) + 1);
	_ContainerPathInfo laRunInfo[lnMaxRunInfo];
	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex < pnIdentities; lnIndex++) {

		size_t lnIdentities = 0;
		size_t lnRunInfoIndex = 0;
		size_t lnMaxDeepness = 0;

		// Step 1: Search through tree to obtain all instances of an identity
		CHECK_OR_RETURN( _FindAllIdentifiedContainers(poThis, laRunInfo, lnMaxRunInfo, &lnIdentities, lnIndex, 0) );

		// Step 1a: If there is 1 instance or less then skip the algo below
		if (lnIdentities <= 1) { continue; }

		// Step 2: Calculating maximum depth. The greater is the depth the complex structure is.
		for (lnRunInfoIndex = 0; lnRunInfoIndex < lnIdentities; lnRunInfoIndex++) {
			lnMaxDeepness = lnMaxDeepness >= laRunInfo[lnRunInfoIndex].nDepth ? lnMaxDeepness : laRunInfo[lnRunInfoIndex].nDepth;
		}

		// Step 3: Reducing items burrowed higher than the deepest one.
		for (lnRunInfoIndex = 0; lnRunInfoIndex < lnIdentities; lnRunInfoIndex++) {
			if (laRunInfo[lnRunInfoIndex].nDepth < lnMaxDeepness) {
				if (laRunInfo[lnRunInfoIndex].oContainer->oParent != NULL) {
					_ContainerRemoveChild(laRunInfo[lnRunInfoIndex].oContainer->oParent, laRunInfo[lnRunInfoIndex].oContainer);
				}
			}
		}
	}

	return OK;
}

static Status _CollectContainerContents(const TextGrid * poGrid, Container * poThis, const TextRect * paRects, const size_t pnRects) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex < poThis->nItems; lnIndex++) {
		CHECK_OR_RETURN( _CollectContainerContents( poGrid, poThis->aItems[lnIndex], paRects, pnRects ) );
	}

	// If this container is a root container then we must to resolve it to proper type
	if (poThis->eType == CONTAINER_TYPE_ROOT) {

		if (poThis->nItems == 0) { poThis->eType = CONTAINER_TYPE_WINDOW; }
		else { poThis->eType = CONTAINER_TYPE_STRIP; }
	}

	if (poThis->eType == CONTAINER_TYPE_WINDOW) {
		// Collecting actual data

		const TextRect * loRect = &(paRects[poThis->nIdentity]);
		const size_t lnPossibleWindowNameSize = (loRect->top.x2 - loRect->top.x1 + 1) * (loRect->left.y2 - loRect->left.y1 + 1);

		FREE(poThis->sName);
		poThis->sName = malloc(lnPossibleWindowNameSize + 1);

		char * lsBuffer = poThis->sName;
		*lsBuffer = 0;

		size_t lnCol = 0;
		size_t lnRow = 0;

		for (lnRow = loRect->left.y1; lnRow<=loRect->left.y2; lnRow++) {

			char * lsBufferOnStride = lsBuffer;

			if (lsBufferOnStride != poThis->sName) {
				*(lsBuffer++) = ' ';
				*(lsBuffer) = 0;
			}

			for (lnCol = loRect->top.x1; lnCol<=loRect->top.x2; lnCol++) {
				const char lcSymbol = poGrid->aData[lnRow][lnCol];
				if ((lcSymbol == ' ') && ((lsBuffer == lsBufferOnStride) || (*(lsBuffer - 1) == ' '))) { continue; }
				if (lcSymbol == '|') { continue; }
				if (lcSymbol == '-') { continue; }
				if (lcSymbol == '\n') { continue; }

				*(lsBuffer++) = lcSymbol;
				*(lsBuffer) = 0;
			}

			while (lsBuffer != poThis->sName) {
				if ((*lsBuffer) == 0) { lsBuffer--; continue; }
				if ((*lsBuffer) == ' ') {lsBuffer--; continue; }
				break;
			}
			if (*lsBuffer != 0) { lsBuffer++; *lsBuffer = 0; }
		}

		*lsBuffer = 0;

	} else {
		// Determining the type of a strip
		if (poThis->nItems > 1) {

			const TextRect * loRect1 = &(paRects[poThis->aItems[0]->nIdentity]);
			const TextRect * loRect2 = &(paRects[poThis->aItems[1]->nIdentity]);

			poThis->eType = (loRect1->top.y1 == loRect2->top.y2)
				? CONTAINER_TYPE_STRIP_H
				: CONTAINER_TYPE_STRIP_V;
		}
	}

	return OK;
}

Status containerRootCreate(Container ** poThis, const TextGrid * poGrid, const TextRect * paRects, const size_t pnRects) {

	const TextRect * loRootRect = NULL;
	size_t lnRootRect = 0;

	CHECK_OR_RETURN_WITH_MESSAGE( _GetRootRect(paRects, pnRects, &loRootRect, &lnRootRect), "Cannot obtain root area for layout.\n" );
	CHECK_OR_RETURN_WITH_MESSAGE( _ContainerCreate(poThis, CONTAINER_TYPE_ROOT, lnRootRect), "Cannot create root container.\n" );
	CHECK_OR_RETURN_WITH_MESSAGE( _FillContainer(poGrid, poThis, loRootRect, lnRootRect, paRects, pnRects), "Cannot fill root container.\n");
	CHECK_OR_RETURN_WITH_MESSAGE( _OptimizeContainer(poGrid, *poThis, pnRects), "Cannot optimize root container data.\n" );
	CHECK_OR_RETURN_WITH_MESSAGE( _CollectContainerContents(poGrid, *poThis, paRects, pnRects), "Cannot collect root container contents data.\n" );

	return OK;
}

Status containerRootPrint(Container * poThis) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "containerRootPrint: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_MESSAGE( _ContainerPrint(poThis, 0), "containerRootPrint: cannot print data.\n" );

    return OK;
}

Status containerRootDestroy(Container ** poThis) {
	return _ContainerDestroy(poThis);
}
