#ifndef _TERMINATORIZE_H_
#define _TERMINATORIZE_H_

/************************************************************\
 * This is a default multiheader for terminatorize C sources
 ************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef enum {
    FALSE = 0,
    TRUE = 1
} Bool;

typedef char Char;
typedef Char * String;

#define ALLOC(t) ((t*)calloc(1, sizeof(t)))
#define FREE(x) { if ((x) != NULL) { free(x); (x) = NULL; } }
#define $(x) #x

#include "runtime.h"
#include "textgrid.h"
#include "textreader.h"
#include "container.h"

#endif /* _TERMINATORIZE_H_ */
