#ifndef _RUNTIME_H_
#define _RUNTIME_H_

/**************************************************************\
 * Composite status definition
 *-------------------------------------------------------------
 * All functions must to return Status in order to proper error
 * handling implementation. Status may include additional info
 * about function run process.
 **************************************************************/

typedef unsigned short Status;

enum {
    OK = 0,
    ERROR = 1 << 7
};

/*************************\
 * Status checking macros
 *************************/

#define IS_ERROR(s) (s & ERROR)
#define IS_OK(s) (!IS_ERROR(s))

/***************************************\
 * Various control flow checking macros
 ***************************************/

#define MESSAGE(m, ...) { \
    printf(m, ##__VA_ARGS__); \
}

#define TRACE(m, ...) { \
    printf("%s [%u]: "m, __FILE__, __LINE__,  ##__VA_ARGS__); \
}

#define ACCUMULATE_STATUS(r, x) r = r | (x)

#define CHECK_OR_RETURN(v) \
{ \
    Status r = (v); \
    if (IS_ERROR(r)) { \
	return r; \
    } \
}

#define _CHECK_OR_RETURN_WITH_HANDLER(v, h, m, ...) \
{ \
    Status r = (v); \
    if (IS_ERROR(r)) {\
	h(m, ##__VA_ARGS__);\
	return r;\
    }\
}

#define CHECK_OR_RETURN_WITH_MESSAGE(v, m, ...) \
    _CHECK_OR_RETURN_WITH_HANDLER(v, MESSAGE, m, ##__VA_ARGS__)

#define CHECK_OR_RETURN_WITH_TRACE(v, m, ...) \
    _CHECK_OR_RETURN_WITH_HANDLER(v, TRACE, m, ##__VA_ARGS__)

#endif /*_RUNTIME_H_*/
