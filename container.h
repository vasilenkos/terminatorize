#ifndef _CONTAINER_H_
#define _CONTAINER_H_

typedef enum {
    CONTAINER_TYPE_ROOT,
    CONTAINER_TYPE_STRIP,
    CONTAINER_TYPE_STRIP_H,
    CONTAINER_TYPE_STRIP_V,
    CONTAINER_TYPE_WINDOW
} ContainerType;

typedef struct Container Container;

typedef struct Container {
    size_t nIdentity;
    Container * oParent;
    ContainerType eType;
    size_t nItems;
    Container ** aItems;
    String sName;
} Container;

Status containerRootCreate(Container ** poThis, const TextGrid * poGrid, const TextRect * paRects, const size_t pnRects);
Status containerRootPrint(Container * poThis);
Status containerRootDestroy(Container ** poThis);

#endif /*_CONTAINER_H_*/
