#ifndef _TEXTREADER_H_
#define _TEXTREADER_H_

typedef enum {

	TEXT_READER_STATE_NULL = 0,
	TEXT_READER_STATE_READ = 1,
	TEXT_READER_STATE_FAILURE = 2

} TextReaderState;

typedef struct {
	String sName;
	String sProfile;
	String sCommand;
} TextReaderWindow;

typedef struct {
	TextGrid oGrid;
	TextReaderWindow * aWindows;
	size_t nWindows;
	String sName;
} TextReaderLayout;

typedef struct {
	TextReaderLayout * aLayouts;
	size_t nLayouts;
    String sFileName;
    TextReaderState eState;
} TextReader;

Status textReaderCreate(TextReader ** poThis, String psFileName);
Status textReaderPerformRead(TextReader * poThis);
Status textReaderDestroy(TextReader ** poThis);

#endif /* _TEXTREADER_H_ */
