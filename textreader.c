#include "terminatorize.h"

Status textReaderCreate(TextReader ** poThis, String psFileName) {

	CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textReaderCreate: poThis must not to be NULL.\n" );
	CHECK_OR_RETURN_WITH_TRACE( *poThis == NULL ? OK : ERROR, "textReaderCreate: poThis must to point on NULL.\n" );
	CHECK_OR_RETURN_WITH_TRACE( psFileName != NULL ? OK : ERROR, "textReaderCreate: psFileName must not to be NULL.\n" );

	TextReader * loThis = (TextReader*)calloc(1, sizeof(TextReader));

    CHECK_OR_RETURN_WITH_TRACE( loThis != NULL ? OK : ERROR, "textReaderCreate: cannot allocate memory for TextReader structure.\n" );

    loThis->sFileName = strdup(psFileName);
    loThis->eState = TEXT_READER_STATE_NULL;
    loThis->nLayouts = 0;
    loThis->aLayouts = NULL;

    if (loThis->sFileName == NULL) {

    	free(loThis);
    	CHECK_OR_RETURN_WITH_TRACE( ERROR, "textReaderCreate: cannot allocate memory for file name.\n" );
    }

    *poThis = loThis;

    return OK;
}

TextReaderLayout * _GetOrCreateLayout(TextReader * poThis, String psName, size_t pnNameLength) {

	size_t lnIndex;
	TextReaderLayout * loResult = NULL;

	for (lnIndex = 0; lnIndex < poThis->nLayouts; lnIndex++) {
		if (TRUE
			&& (strncmp(poThis->aLayouts[lnIndex].sName, psName, pnNameLength) == 0)
			&& (strlen(poThis->aLayouts[lnIndex].sName) == pnNameLength)
		) {
			loResult = &(poThis->aLayouts[lnIndex]);
			break;
		}
	}

	if (loResult == NULL) {
		lnIndex = poThis->nLayouts++;
		poThis->aLayouts = realloc(poThis->aLayouts, sizeof(TextReaderLayout) * poThis->nLayouts);

		loResult = &(poThis->aLayouts[lnIndex]);

		loResult->aWindows = NULL;
		loResult->nWindows = 0;
		loResult->oGrid.nHeight = 0;
		loResult->oGrid.nWidth = 0;
		loResult->oGrid.nMaxWidth = TEXT_GRID_MAX_WIDTH;
		loResult->oGrid.nMaxHeight = TEXT_GRID_MAX_HEIGHT;

		loResult->sName = strndup(psName, pnNameLength);
	}

	return loResult;
}

TextReaderWindow * _GetOrCreateWindow(TextReaderLayout * poThis, String psName, size_t pnNameLength) {

	size_t lnIndex;
	TextReaderWindow * loResult = NULL;

	for (lnIndex = 0; lnIndex < poThis->nWindows; lnIndex++) {
		if (TRUE
			&& (strncmp(poThis->aWindows[lnIndex].sName, psName, pnNameLength) == 0)
			&& (strlen(poThis->aWindows[lnIndex].sName) == pnNameLength)
		) {
			loResult = &(poThis->aWindows[lnIndex]);
			break;
		}
	}

	if (loResult == NULL) {
		lnIndex = poThis->nWindows++;
		poThis->aWindows = realloc(poThis->aWindows, sizeof(TextReaderWindow) * poThis->nWindows);

		loResult = &(poThis->aWindows[lnIndex]);

		loResult->sCommand = NULL;
		loResult->sName = strndup(psName, pnNameLength);
		loResult->sProfile = NULL;
	}

	return loResult;
}

#define _SKIP_WHITE_SPACE(buf, pos, len) \
while (pos < len) { \
	if (buf[pos] == ' ') { pos++; continue; } \
	if (buf[pos] == '\t') { pos++; continue; } \
	break; \
}

#define _GET_COMMAND_START_AND_LENGTH(buf, pos, len, cmd_start, cmd_len) \
cmd_start = pos; \
while (pos < len) { \
	if (buf[pos] == '\n') { if (cmd_len > 0) cmd_len--; break; } \
	if (buf[pos] == 0) { if (cmd_len > 0) cmd_len--; break; } \
	pos++; \
	cmd_len++; \
}

Status textReaderPerformRead(TextReader * poThis) {

	CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textReaderPerformRead: poThis must not to be NULL.\n" );
	CHECK_OR_RETURN_WITH_TRACE( poThis->eState == TEXT_READER_STATE_NULL ? OK : ERROR, "textReaderPerformRead: text reader state must to be %s.\n", $(TEXT_READER_STATE_NULL) );

	FILE * loFile = NULL;
	const size_t lnCommandBuffer = 512;
	Char laCommandBuffer[lnCommandBuffer + 1];
	size_t lnLine = 0;
	enum {
		PARSING_STATE_NONE,
		PARSING_STATE_LAYOUT,
		PARSING_STATE_WINDOW,
		PARSING_STATE_GRID
	} leParsingState = PARSING_STATE_NONE;

	TextReaderLayout * loLayout = NULL;
	TextReaderWindow * loWindow = NULL;

	loFile = fopen(poThis->sFileName, "r");

	if (loFile == NULL) {
		poThis->eState = TEXT_READER_STATE_FAILURE;
		MESSAGE( "Cannot open input file %s.\n", poThis->sFileName );
		return ERROR;
	}

	// 1. File reading
	while (TRUE) {

		if (feof(loFile)) { break; }

		size_t lnCommandPosition = 0;

		if (fgets(laCommandBuffer, lnCommandBuffer, loFile) == NULL) { break; }
		lnLine++;
		laCommandBuffer[lnCommandBuffer] = 0;

		_SKIP_WHITE_SPACE(laCommandBuffer, lnCommandPosition, lnCommandBuffer);

		// Comment
		if (FALSE
			|| (laCommandBuffer[lnCommandPosition] == '#')
		) { continue; }

		// Empty line resets parsing state to `COMMANDS`
		if (FALSE
			|| (laCommandBuffer[lnCommandPosition] == '0')
			|| (laCommandBuffer[lnCommandPosition] == '\n')
		) {
			leParsingState = PARSING_STATE_NONE;
			loLayout = NULL;
			continue;
		}

		// Layout
		if (TRUE
			&& (FALSE
				|| (leParsingState == PARSING_STATE_NONE)
				|| (leParsingState == PARSING_STATE_GRID)
				|| (leParsingState == PARSING_STATE_LAYOUT)
			)
			&& (strncmp(&(laCommandBuffer[lnCommandPosition]), "layout ", 7) == 0)
		) {
			lnCommandPosition += 7;

			size_t lnCommandLength = 0;
			size_t lnCommandStart = 0;

			_SKIP_WHITE_SPACE(laCommandBuffer, lnCommandPosition, lnCommandBuffer);
			_GET_COMMAND_START_AND_LENGTH(laCommandBuffer, lnCommandPosition, lnCommandBuffer, lnCommandStart, lnCommandLength);

			loLayout = _GetOrCreateLayout(poThis, &(laCommandBuffer[lnCommandStart]), lnCommandLength);
			loWindow = NULL;
			leParsingState = PARSING_STATE_LAYOUT;

			continue;
		}

		// Window allocation command
		if (TRUE
			&& (FALSE
				|| (leParsingState == PARSING_STATE_LAYOUT)
				|| (leParsingState == PARSING_STATE_WINDOW)
			)
			&& (strncmp(&(laCommandBuffer[lnCommandPosition]), "window ", 7) == 0)
			&& (loLayout != NULL)
		) {
			lnCommandPosition += 7;

			size_t lnCommandLength = 0;
			size_t lnCommandStart = 0;

			_SKIP_WHITE_SPACE(laCommandBuffer, lnCommandPosition, lnCommandBuffer);
			_GET_COMMAND_START_AND_LENGTH(laCommandBuffer, lnCommandPosition, lnCommandBuffer, lnCommandStart, lnCommandLength);

			loWindow = _GetOrCreateWindow(loLayout, &(laCommandBuffer[lnCommandStart]), lnCommandLength);
			leParsingState = PARSING_STATE_WINDOW;

			continue;
		}

		// Profile
		if (TRUE
			&& (leParsingState == PARSING_STATE_WINDOW)
			&& (strncmp(&(laCommandBuffer[lnCommandPosition]), "profile ", 8) == 0)
			&& (loWindow != NULL)
		) {
			lnCommandPosition += 8;

			size_t lnCommandLength = 0;
			size_t lnCommandStart = 0;

			_SKIP_WHITE_SPACE(laCommandBuffer, lnCommandPosition, lnCommandBuffer);
			_GET_COMMAND_START_AND_LENGTH(laCommandBuffer, lnCommandPosition, lnCommandBuffer, lnCommandStart, lnCommandLength);

			FREE(loWindow->sProfile);
			loWindow->sProfile = strndup(&(laCommandBuffer[lnCommandStart]), lnCommandLength);

			continue;
		}

		// Command
		if (TRUE
			&& (leParsingState == PARSING_STATE_WINDOW)
			&& (strncmp(&(laCommandBuffer[lnCommandPosition]), "command ", 8) == 0)
			&& (loWindow != NULL)
		) {
			lnCommandPosition += 8;

			size_t lnCommandLength = 0;
			size_t lnCommandStart = 0;

			_SKIP_WHITE_SPACE(laCommandBuffer, lnCommandPosition, lnCommandBuffer);
			_GET_COMMAND_START_AND_LENGTH(laCommandBuffer, lnCommandPosition, lnCommandBuffer, lnCommandStart, lnCommandLength);

			FREE(loWindow->sCommand);
			loWindow->sCommand = strndup(&(laCommandBuffer[lnCommandStart]), lnCommandLength);

			continue;
		}

		// Grid command
		if (TRUE
			&& (FALSE
				|| (leParsingState == PARSING_STATE_WINDOW)
				|| (leParsingState == PARSING_STATE_LAYOUT)
			)
			&& (strncmp(&(laCommandBuffer[lnCommandPosition]), "grid", 4) == 0)
			&& (FALSE
				|| (laCommandBuffer[lnCommandPosition + 4] == ' ')
				|| (laCommandBuffer[lnCommandPosition + 4] == '\n')
			)
		) {
			leParsingState = PARSING_STATE_GRID;
			loWindow = NULL;
			continue;
		}

		// Grid lines
		if (TRUE
			&& (leParsingState == PARSING_STATE_GRID)
			&& (FALSE
				|| (laCommandBuffer[lnCommandPosition + 0] == '-')
				|| (laCommandBuffer[lnCommandPosition + 0] == '|')
			)
		) {
			size_t lnCommandLength = 0;
			TextGrid * loGrid = &(loLayout->oGrid);
			String lsGridWriter = loGrid->aData[loGrid->nHeight];

			if (loGrid->nHeight >= loGrid->nMaxHeight) {
				MESSAGE("Too much lines of grid in input file. Limit of %u exceeded.\n", loGrid->nMaxHeight);
				break;
			}

			lsGridWriter[0] = 0;

			while (laCommandBuffer[lnCommandPosition] && (laCommandBuffer[lnCommandPosition] != '\n' ) && (lnCommandLength < loGrid->nMaxWidth)) {
				*(lsGridWriter) = laCommandBuffer[lnCommandPosition];
				lnCommandLength ++;
				lnCommandPosition ++;
				lsGridWriter++;
			}
			*lsGridWriter = 0;

			if (loGrid->nWidth < lnCommandLength) { loGrid->nWidth = lnCommandLength; }

			loGrid->nHeight++;
			continue;
		}

		poThis->eState = TEXT_READER_STATE_FAILURE;
		MESSAGE( "Unexpected sequence found in input on line %u: %s", lnLine, &(laCommandBuffer[lnCommandPosition]) );
		fclose(loFile);
		return ERROR;
	}

	fclose(loFile);

	// 2. Postprocessing
	size_t lnIndex = 0;
	for (lnIndex = 0; lnIndex < poThis->nLayouts; lnIndex++) {
		CHECK_OR_RETURN_WITH_MESSAGE( textGridNormalize(&(poThis->aLayouts[lnIndex].oGrid)), "Cannot normalize text grid of layout `%s`. File may be erroneous.\n", poThis->aLayouts[lnIndex].sName );
	}

	poThis->eState = TEXT_READER_STATE_READ;

	return OK;
}

Status textReaderDestroy(TextReader ** poThis) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textReaderDestroy: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( *poThis != NULL ? OK : ERROR, "textReaderDestroy: poThis must not to point on NULL.\n" );

    TextReader * loThis = *poThis;
    size_t lnIndex = 0;
	size_t lnIndex2 = 0;

    for (lnIndex = 0; lnIndex < loThis->nLayouts; lnIndex++) {

    	FREE(loThis->aLayouts[lnIndex].sName);

        for (lnIndex2 = 0; lnIndex2 < loThis->aLayouts[lnIndex].nWindows; lnIndex2++) {
        	FREE(loThis->aLayouts[lnIndex].aWindows[lnIndex].sName);
        	FREE(loThis->aLayouts[lnIndex].aWindows[lnIndex].sCommand);
        	FREE(loThis->aLayouts[lnIndex].aWindows[lnIndex].sProfile);
        }

    	FREE(loThis->aLayouts[lnIndex].aWindows);
    }

	FREE(loThis->sFileName);
    FREE(loThis);

    *poThis = NULL;

    return OK;
}
