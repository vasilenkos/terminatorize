#include "terminatorize.h"

Status entryPoint(int pnArgc, const char** const paArgv) {

    TextReader * loReader = NULL;
    size_t lnIndex = 0;

    CHECK_OR_RETURN( textReaderCreate(&loReader, "./input.txt") );
    CHECK_OR_RETURN( textReaderPerformRead(loReader) );
	CHECK_OR_RETURN_WITH_MESSAGE( loReader->eState == TEXT_READER_STATE_READ ? OK : ERROR, "Cannot read input.\n" );

    for (lnIndex = 0; lnIndex < loReader->nLayouts; lnIndex++)
    { /* We've successfully read data from input */

		const TextGrid * loGrid = &(loReader->aLayouts[lnIndex].oGrid);

		{ /* Text grid has been parsed */

			const size_t lnLinesMax = 256;
			TextLine laLines[lnLinesMax];
			size_t lnLines = 0;

			CHECK_OR_RETURN( textGridPrint(loGrid) );
			CHECK_OR_RETURN( textGridTrace(loGrid, laLines, lnLinesMax, &lnLines) );
			CHECK_OR_RETURN( textGridLinesPrint(loGrid, laLines, lnLines) );

			{ /* Grid has been traced and lines are ready */

				const size_t lnRectsMax = 256;
				TextRect laRects[lnRectsMax];
				size_t lnRects = 0;

				CHECK_OR_RETURN( textGridLinesRectangulate(loGrid, laLines, lnLines, laRects, lnRectsMax, &lnRects) );
				CHECK_OR_RETURN( textGridRectsPrint(loGrid, laRects, lnRects) );

				{ /* Grid lines had been rectangulated and rects are ready */

					Container * loRoot = NULL;
					CHECK_OR_RETURN( containerRootCreate(&loRoot, loGrid, laRects, lnRects) );
					CHECK_OR_RETURN( containerRootPrint(loRoot) );
					CHECK_OR_RETURN( containerRootDestroy(&loRoot) );
				}
			}
		}
    }

    CHECK_OR_RETURN( textReaderDestroy(&loReader) );

    return OK;
}

int main(int pnArgc, const char** const paArgv) {

    return IS_OK(entryPoint(pnArgc, paArgv)) ? 0 : 1;
}
