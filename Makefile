CFLAGS += -Wall -pthread
LIBS += -pthread

all: exe 

exe: main.c
	$(CC) $(CFLAGS) -o terminatorize container.c textreader.c textgrid.c main.c
	chmod +x ./terminatorize

clean:
	rm -f *.o
