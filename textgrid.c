#include "terminatorize.h"

Status textGridNormalize(TextGrid * poThis) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textGridNormalize: poThis must not to be NULL.\n" );

	size_t lnRow = 0;

	for (lnRow = 0; lnRow < poThis->nHeight; lnRow++ ) {

		String lsLineBuffer = poThis->aData[lnRow];
		size_t lnColumn = 0;

		while ((lsLineBuffer[lnColumn] != 0) && (lnColumn < poThis->nWidth) && ( lnColumn < poThis->nMaxWidth)) { lnColumn++; }
		while (lnColumn < poThis->nWidth) { lsLineBuffer[lnColumn] = ' '; lnColumn++; }
		lsLineBuffer[lnColumn] = 0;
	}

	return OK;
}

Status textGridPrint(const TextGrid * poThis) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textGridPrint: poThis must not to be NULL.\n" );

	size_t lnRow = 0;

	for (lnRow = 0; lnRow < poThis->nHeight; lnRow++ ) { MESSAGE( "%3u: %s\n", lnRow, poThis->aData[lnRow]); }

	return OK;
}

#define _ADDLINE(l, t, n, _x1, _y1, _x2, _y2) \
{ \
	CHECK_OR_RETURN_WITH_MESSAGE( n < t ? OK : ERROR, "Cannot add traced line: not enough space in buffer (%u lines acquired).\n", n ); \
	l[n].x1 = _x1; \
	l[n].y1 = _y1; \
	l[n].x2 = _x2; \
	l[n].y2 = _y2; \
	(n)++; \
	if (!_LineIsUnique(&(l[(n-1)]), l, (n)-1)) { (n)--; } \
}

static Bool _LineIsUnique(const TextLine * poLine, TextLine * paLines, const size_t pnLines) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex < pnLines; lnIndex++) {

		if (memcmp(poLine, &(paLines[lnIndex]), sizeof(TextLine)) == 0) {
			return FALSE;
		}
	}

	return TRUE;
}

Status textGridTrace(const TextGrid * poThis, TextLine * paLines, const size_t pnLines, size_t * pnActualLines) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textGridTrace: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( paLines != NULL ? OK : ERROR, "textGridTrace: paLines must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( pnActualLines != NULL ? OK : ERROR, "textGridTrace: pnActualLines must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( poThis->nWidth > 0 ? OK : ERROR, "textGridTrace: grid must not to be empty.\n" );
    CHECK_OR_RETURN_WITH_TRACE( poThis->nHeight > 0 ? OK : ERROR, "textGridTrace: grid must not to be empty.\n" );

	size_t lnRow = 0;
	size_t lnColumn = 0;

	// 0. Boundaries

	_ADDLINE(paLines, pnLines, *pnActualLines, 0, 0, poThis->nWidth-1, 0);
	_ADDLINE(paLines, pnLines, *pnActualLines, 0, 0, 0, poThis->nHeight - 1);
	_ADDLINE(paLines, pnLines, *pnActualLines, poThis->nWidth-1, 0, poThis->nWidth-1, poThis->nHeight - 1);
	_ADDLINE(paLines, pnLines, *pnActualLines, 0, poThis->nHeight - 1, poThis->nWidth-1, poThis->nHeight - 1);

	// 1. Width-cut
	for (lnRow = 0; lnRow < poThis->nHeight; lnRow++ ) {

		lnColumn = 0;
		size_t lnColumnFix = 0;

		while (TRUE) {
			if (lnColumn >= poThis->nMaxWidth) {
				if (lnColumnFix != lnColumn) {
					size_t lnCF = lnColumnFix;
					size_t lnC = lnColumn - 1;
					if ((lnCF > 0) && (poThis->aData[lnRow][lnCF-1] == '|')) { lnCF--; }

					_ADDLINE(paLines, pnLines, *pnActualLines, lnCF, lnRow, lnC, lnRow);
				}
				break;
			}
			if (poThis->aData[lnRow][lnColumn] != '-') {
				if (lnColumnFix != lnColumn) {
					size_t lnCF = lnColumnFix;
					size_t lnC = lnColumn - 1;
					if ((lnCF > 0) && (poThis->aData[lnRow][lnCF-1] == '|')) { lnCF--; }
					if (((lnC + 1) < poThis->nWidth) && (poThis->aData[lnRow][lnC+1] == '|')) { lnC++; }
					_ADDLINE(paLines, pnLines, *pnActualLines, lnCF, lnRow, lnC, lnRow);
				}
				lnColumnFix = lnColumn + 1;
			}
			lnColumn++;
		}
	}

	// 2. Height-cut
	for (lnColumn = 0; lnColumn < poThis->nWidth; lnColumn++ ) {

		lnRow = 0;
		size_t lnRowFix = 0;

		while (TRUE) {
			if (lnRow >= poThis->nMaxHeight) {
				if (lnRowFix != lnRow) {
					size_t lnRF = lnRowFix;
					size_t lnR = lnRow - 1;
					if ((lnRF > 0) && (poThis->aData[lnRF-1][lnColumn] == '-')) { lnRF--; }
					_ADDLINE(paLines, pnLines, *pnActualLines, lnColumn, lnRF, lnColumn, lnR);
				}
				break;
			}
			if (poThis->aData[lnRow][lnColumn] != '|') {
				if (lnRowFix != lnRow) {
					size_t lnRF = lnRowFix;
					size_t lnR = lnRow - 1;
					if ((lnRF > 0) && (poThis->aData[lnRF-1][lnColumn] == '-')) { lnRF--; }
					if (((lnR + 1) < poThis->nHeight) && (poThis->aData[lnR+1][lnColumn] == '-')) { lnR++; }
					_ADDLINE(paLines, pnLines, *pnActualLines, lnColumn, lnRF, lnColumn, lnR);
				}
				lnRowFix = lnRow + 1;
			}
			lnRow++;
		}
	}

	return OK;
}

#undef _ADDLINE

Status textGridLinesPrint(const TextGrid * poThis, const TextLine * paLines, const size_t pnLines) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textLinesPrint: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( paLines != NULL ? OK : ERROR, "textLinesPrint: paLines must not to be NULL.\n" );

    size_t lnIndex = 0;

    for (lnIndex = 0; lnIndex < pnLines; lnIndex++) {
        MESSAGE("%3u: [%2u,%2u,%2u,%2u]%s", lnIndex, paLines[lnIndex].x1, paLines[lnIndex].y1, paLines[lnIndex].x2, paLines[lnIndex].y2, ((lnIndex > 0) && (((lnIndex + 1) % 4 == 0) || ((lnIndex+1) == pnLines))) ? "\n" : " ");
    }

    return OK;
}

static Bool _FirstHLineCoversSecondOne(const TextLine * poFirst, const TextLine * poSecond) {
	return TRUE
		&& (poFirst->x1 <= poSecond->x1)
		&& (poFirst->y1 == poSecond->y1)
		&& (poFirst->x2 >= poSecond->x2)
		&& (poFirst->y2 == poSecond->y2);
}

static Bool _FirstVLineCoversSecondOne(const TextLine * poFirst, const TextLine * poSecond) {
	return TRUE
		&& (poFirst->x1 == poSecond->x1)
		&& (poFirst->y1 <= poSecond->y1)
		&& (poFirst->x2 == poSecond->x2)
		&& (poFirst->y2 >= poSecond->y2);
}

static Bool _RectIsSurrounded(TextRect * poRect, const TextLine * paLines, const size_t pnLines) {

	size_t lnIndex = 0;

	struct {
		unsigned short top:1;
		unsigned short right:1;
		unsigned short bottom:1;
		unsigned short left:1;
	} loState = { .top = 0, .right = 0, .bottom = 0, .left = 0 };

	for (lnIndex = 0; lnIndex < pnLines; lnIndex++) {
		loState.top |= _FirstHLineCoversSecondOne(&(paLines[lnIndex]), &(poRect->top));
		loState.right |= _FirstVLineCoversSecondOne(&(paLines[lnIndex]), &(poRect->right));
		loState.bottom |= _FirstHLineCoversSecondOne(&(paLines[lnIndex]), &(poRect->bottom));
		loState.left |= _FirstVLineCoversSecondOne(&(paLines[lnIndex]), &(poRect->left));

		if (loState.top && loState.right && loState.bottom && loState.left) {
			return TRUE;
		}
	}

	return FALSE;
}

#define _CP_LINE(d, s) { \
	(d).x1 = (s).x1; \
	(d).y1 = (s).y1; \
	(d).x2 = (s).x2; \
	(d).y2 = (s).y2; \
}

#define _SET_LINE(d, _x1, _y1, _x2, _y2) { \
	(d).x1 = _x1; \
	(d).y1 = _y1; \
	(d).x2 = _x2; \
	(d).y2 = _y2; \
}

static Bool _RectIsUnique(const TextRect * poRect, const TextRect * paRects, const size_t pnRects) {

	size_t lnIndex = 0;

	for (lnIndex = 0; lnIndex < pnRects; lnIndex++) {
		if (memcmp(poRect, &(paRects[lnIndex]), sizeof(TextRect)) == 0) {
			return FALSE;
		}
	}

	return TRUE;
}

static Status _TextLinesRectangulateDirectionalExpand(const TextGrid * poThis, TextRect * paRects, const size_t pnRects, size_t * pnActualRects, const TextLine * paLines, const size_t pnLines, const size_t pnLine, const int pnDirection) {

	const TextLine * loLine = &(paLines[pnLine]);
	TextRect loRect;
	Bool lbIsVertical = loLine->x1 == loLine->x2;
	Bool lbGravityToRightDown = pnDirection >= 0;
	size_t lnSteps = 0;
	size_t lnMaxSteps = 0;

	if (lbIsVertical) {
		_CP_LINE(loRect.left, *loLine);
		_SET_LINE(loRect.top, loLine->x1, loLine->y1, loLine->x1, loLine->y1);
		_SET_LINE(loRect.bottom, loLine->x2, loLine->y2, loLine->x2, loLine->y2);
		_CP_LINE(loRect.right, *loLine);

		lnMaxSteps = lbGravityToRightDown
			? (poThis->nWidth - loLine->x1)
			: (loLine->x1);

	} else {
		_SET_LINE(loRect.left, loLine->x1, loLine->y1, loLine->x1, loLine->y1);
		_CP_LINE(loRect.top, *loLine);
		_CP_LINE(loRect.bottom, *loLine);
		_SET_LINE(loRect.right, loLine->x2, loLine->y2, loLine->x2, loLine->y2);

		lnMaxSteps = lbGravityToRightDown
			? (poThis->nHeight - loLine->y1)
			: (loLine->y1);
	}

	while (TRUE) {

		if (lnSteps > 0) {
			if (_RectIsSurrounded(&loRect, paLines, pnLines)) {
				if (_RectIsUnique(&loRect, paRects, *pnActualRects)) {
					CHECK_OR_RETURN_WITH_MESSAGE( (*pnActualRects) < pnRects ? OK : ERROR, "Cannot store traced rect: not enough space in buffer (%u rects acquired).\n", *pnActualRects );
					memcpy(&(paRects[*pnActualRects]), &loRect, sizeof(TextRect));
					(*pnActualRects)++;
				}
			}
		}

		if (lnSteps >= lnMaxSteps) { break; }

		if (lbGravityToRightDown) {
			if (lbIsVertical) { loRect.top.x2++; loRect.bottom.x2++; loRect.right.x1++; loRect.right.x2++; }
			else { loRect.bottom.y1++; loRect.bottom.y2++; loRect.left.y2++; loRect.right.y2++; }
		} else {
			if (lbIsVertical) { loRect.top.x1--; loRect.bottom.x1--; loRect.left.x1--; loRect.left.x2--; }
			else { loRect.top.y1--; loRect.top.y2--; loRect.left.y1--; loRect.right.y1--; }
		}

		lnSteps++;
	}

	return OK;
}

Status textGridLinesRectangulate(const TextGrid * poThis, const TextLine * paLines, const size_t pnLines, TextRect * paRects, const size_t pnRects, size_t * pnActualRects) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textLinesRectangulate: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( paLines != NULL ? OK : ERROR, "textLinesRectangulate: paLines must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( paRects != NULL ? OK : ERROR, "textLinesRectangulate: paRects must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( pnActualRects != NULL ? OK : ERROR, "textLinesRectangulate: pnActualRects must not to be NULL.\n" );

    size_t lnCurrentLine;

    for (lnCurrentLine = 0; lnCurrentLine < pnLines; lnCurrentLine++) {
    	CHECK_OR_RETURN( _TextLinesRectangulateDirectionalExpand(poThis, paRects, pnRects, pnActualRects, paLines, pnLines, lnCurrentLine, -1) );
    	CHECK_OR_RETURN( _TextLinesRectangulateDirectionalExpand(poThis, paRects, pnRects, pnActualRects, paLines, pnLines, lnCurrentLine, 1) );
    }

	return OK;
}

Status textGridRectsPrint(const TextGrid * poThis, const TextRect * paRects, const size_t pnRects) {

    CHECK_OR_RETURN_WITH_TRACE( poThis != NULL ? OK : ERROR, "textRectsPrint: poThis must not to be NULL.\n" );
    CHECK_OR_RETURN_WITH_TRACE( paRects != NULL ? OK : ERROR, "textRectsPrint: paRects must not to be NULL.\n" );

    size_t lnIndex = 0;

    for (lnIndex = 0; lnIndex < pnRects; lnIndex++) {

    	size_t lnR = 0;
    	size_t lnC = 0;

		MESSAGE("%3u:\n", lnIndex);

		for (lnR = 0; lnR < poThis->nHeight; lnR++) {
			for (lnC = 0; lnC < poThis->nWidth; lnC++) {
				if (TRUE
					&& (lnR >= paRects[lnIndex].left.y1)
					&& (lnR <= paRects[lnIndex].left.y2)
					&& (lnC >= paRects[lnIndex].left.x1)
					&& (lnC <= paRects[lnIndex].right.x1)
				) { MESSAGE("#"); }
				else { MESSAGE("."); }
			}
			MESSAGE("\n");
		}
    }

	return OK;
}
