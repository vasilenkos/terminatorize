#ifndef _TEXTGRID_H_
#define _TEXTGRID_H_

#define TEXT_GRID_MAX_HEIGHT 64
#define TEXT_GRID_MAX_WIDTH 80

typedef struct {

	Char aData[TEXT_GRID_MAX_HEIGHT][TEXT_GRID_MAX_WIDTH + 1];
    size_t nHeight;
    size_t nWidth;
	size_t nMaxHeight;
	size_t nMaxWidth;

} TextGrid;

typedef struct {

	size_t x1;
	size_t y1;
	size_t x2;
	size_t y2;

} TextLine;

typedef struct {

	TextLine top;
	TextLine right;
	TextLine bottom;
	TextLine left;

} TextRect;

Status textGridNormalize(TextGrid * poThis);
Status textGridPrint(const TextGrid * poThis);
Status textGridTrace(const TextGrid * poThis, TextLine * paLines, const size_t pnLines, size_t * pnActualLines);
Status textGridLinesPrint(const TextGrid * poThis, const TextLine * paLines, const size_t pnLines);
Status textGridLinesRectangulate(const TextGrid * poThis, const TextLine * paLines, const size_t pnLines, TextRect * paRects, const size_t pnRects, size_t * pnActualRects);
Status textGridRectsPrint(const TextGrid * poThis, const TextRect * paRects, const size_t pnRects);

#endif /* _TEXTGRID_H_ */
